import io
import os
import sys

class Cleaner() :
	def __init__(self) :
		self.written = 0
		self.old_message = ''
	def consoleSpam(self, inc ):
		self.written += inc
		message = str(self.written)
		sys.stdout.write('\r' + (' ' * len(self.old_message)))
		sys.stdout.write('\rProcessing... ' + message)
		old_message = message
		
	def randomWrite(self,filename) :
		size = 2048
		array = bytearray(os.urandom(size))
		f = io.FileIO(filename,"w" )
		l = 1
		self.written = 0
		try:
			while l > 0:
				l = f.write(array)
				f.flush()
				self.consoleSpam( l )
		except IOError :
			print(" done!")
		
		f.close()
		os.remove(filename)
			
def start(filename,times):
	c = Cleaner()
	i = 0
	while i < times :
		print( "Executing: " + str(i) )
		c.randomWrite(filename)
		i += 1
		
