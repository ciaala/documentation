from PIL import Image, ImageOps


def rename(path, size):
    tokens = path.split('/')
    name = '/'.join(tokens[:-1]) + "/" + "x".join([str(x) for x in size]) + "_" + tokens[-1]
    return name


def thumbnail(path, size):
    with Image.open(path) as image:
        isize = image.size
        if isize[0] > size[0] or isize[1] > size[1]:
            thumb = ImageOps.fit(image, size, Image.ANTIALIAS)
            name = rename(path, size)
            thumb.save(name)


def resize(path, size):
    with Image.open(path) as image:
        isize = image.size
        if isize[0] > size[0] or isize[1] > size[1]:
            image.thumbnail(size, Image.ANTIALIAS)
            name = rename(path, size)
            image.save(name)

if __name__ == "__main__":
    thumbnail("C:/Users/crypt/Desktop/test_pil/wester-movie-set.jpg", (128, 128))
    thumbnail("C:/Users/crypt/Desktop/test_pil/wester-movie-set.jpg", (256, 256))
    thumbnail("C:/Users/crypt/Desktop/test_pil/wester-movie-set.jpg", (512, 512))
    resize("C:/Users/crypt/Desktop/test_pil/wester-movie-set.jpg", (1024, 1024))
    resize("C:/Users/crypt/Desktop/test_pil/wester-movie-set.jpg", (2048, 2048))
