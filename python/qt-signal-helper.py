"""print(helper.connection("objectEditor","object_type","signal_object_type","setText","QString"))"""
def glo(logic, g_signal, g_slot, params, prefix, guis):
    v=""
    s=""
    for g in guis:
        generic = prefix+g
        v += smart(logic, g_signal, g_slot, params,  g, generic)
        s += slots(g, prefix, params)
    return v, s

def slots(gui, prefix, params ):
    a = "<slot>"+prefix+gui+"("+params+")</slot>\n"
    b = "<signal>signal_"+prefix+gui+"("+params+")</signal>\n"
    return a+b
    
def smart(logic, g_signal, g_slot, params,  gui, generic):
    pair1= (g_signal,  generic)
    combo = "signal_"+generic
    pair2 = ( combo, g_slot)
    return gen(gui, logic, pair1, pair2, params)
    
def gen( sender, receiver,  pair1,  pair2,  params) :
    signal, slot =pair1
    a = connection(sender, receiver,  signal, slot, params)
    signal, slot =pair2
    b = connection(receiver,sender,  signal, slot, params)
    return a+b
    
def connection(sender,receiver,signal,slot,params ):
  c =  """
  <connection>
   <sender>%s</sender>
   <signal>%s(%s)</signal>
   <receiver>%s</receiver>
   <slot>%s(%s)</slot>
   <hints>
    <hint type=\"sourcelabel\">
     <x>10</x>
     <y>10</y>
    </hint>
    <hint type=\"destinationlabel\">
     <x>10</x>
     <y>10</y>
    </hint>
   </hints>
  </connection>
  """
  c = c %(sender,signal,params,receiver,slot,params)
  return c
  
  
  
