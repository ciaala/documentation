#!/bin/env python3

import binascii
import sys
import zipfile
import os


class Checker:

    def __init__(self, base: str, replace: str):
        self.base = base
        self.replace = replace

    def get_file_crc32(self, filename: str):
        with open(self.get_actual_filename(filename), "rb") as sourcefile:
            file_content = sourcefile.read()
            return binascii.crc32(file_content)

    def get_actual_filename(self, filename):
        return filename.replace(self.base, self.replace, 1)

    def get_file_size(self, filename: str):
        stat_info = os.stat(self.get_actual_filename(filename))
        return stat_info.st_size

    def extract_file_crc32_from_zipfile(self, zip_filename: str):
        size_problem = []
        content_problem = []
        if zipfile.is_zipfile(zip_filename):
            with zipfile.ZipFile(zip_filename) as archive:
                for info in archive.infolist():  # type: zipfile.ZipInfo
                    if info.file_size > 0:
                        original_file_size = self.get_file_size(info.filename)
                        if info.file_size != original_file_size:
                            size_problem.append(("!~", info.filename, info.file_size, original_file_size))
                        else:
                            actual_crc = self.get_file_crc32(info.filename)
                            if info.CRC != actual_crc:
                                content_problem.append(("!=", info.filename, info.CRC, actual_crc))
        for element in size_problem:
            print('%s %s %d %d' % element)
        for element in content_problem:
            print('%s %s %d %d' % element)


if __name__ == '__main__':
    zip_filename = sys.argv[1]
    folder = sys.argv[2]
    base = sys.argv[3]
    replace = sys.argv[2]
    print('Comparing content of:\n\t', zip_filename, '\nwith the folder:\n\t', folder)
    checker = Checker(base, replace)
    checker.extract_file_crc32_from_zipfile(zip_filename)
    exit(0)
