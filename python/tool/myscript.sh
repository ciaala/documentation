#!/bin/sh
IFS="
"
for f  in `unzip -v -l RollerMadness.zip | cut -c49- | grep RollerMadness`; 
do
    if [ -f "${f:10}" ]; then 
        crc32=`crc32 "${f:10}" | cut -c-8`
        echo ${crc32}
        if [ "${crc32}" -ne "${f:0:9}" ]; then
            echo "${f:10}"
        fi
    fi
done
